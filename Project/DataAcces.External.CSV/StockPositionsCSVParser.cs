﻿using System.Globalization;

using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic.FileIO;

using Project.API.Models;
using Project.DataAccess.External.CSV.Interfaces;

namespace Project.DataAccess.External.CSV
{
    /// <summary>
    /// Class for parsing csv file into stock records
    /// </summary>
    public class StockPositionsCSVParser : IStockPositionsCSVParser
    {
        // just log information
        private readonly ILogger logger;
        // every row has to have exactly 8 words for valid stock record
        private const int FieldsCount = 8;
        public StockPositionsCSVParser(ILogger<StockPositionsCSVParser> logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Parse csv file and create stock record from every line
        /// Returns list of stock records
        /// </summary>
        public IList<StockPositionRecord> Parse(byte[] csv)
        {
            var lines = GetTextLines(csv);
            lines = GetCleanedCSVLines(lines);
            var stockRecords = new List<StockPositionRecord>();
            foreach (var line in lines)
            {
                try
                {
                    var record = Parse(line);
                    stockRecords.Add(record);
                }
                catch (Exception ex)
                {
                    //e.g. "DREYFUS GOVT CASH MAN INS" has no ticker
                    logger.LogInformation($"Object representation for line '{line}' " +
                        $"could not be created when parsing because of malformed input. " +
                        $"The line will be skipped. Details: {ex.Message}");
                }
            }
            return stockRecords;
        }

        /// <summary>
        /// Divides file by lines and saves it into array
        /// </summary>
        private List<string> GetTextLines(byte[] csv)
        {
            List<string> lines = new List<string>();
            using var ms = new MemoryStream(csv);
            using var reader = new StreamReader(ms);
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                lines.Add(line);
            }
            return lines;
        }

        /// <summary>
        /// Removes first and last line which are header and footer from specified file
        /// </summary>
        private List<string> GetCleanedCSVLines(List<string> lines)
        {
            lines.RemoveAt(0);
            lines.RemoveAt(lines.Count - 1);
            return lines;
        }

        /// <summary>
        /// Divide line by comma (except these in quotes) into field
        /// </summary>
        private List<string> ParseIntoFields(string line)
        {
            using var parser = new TextFieldParser(new StringReader(line));
            parser.HasFieldsEnclosedInQuotes = true;
            parser.SetDelimiters(",");
            var fields = parser.ReadFields();
            return fields.ToList();
        }

        /// <summary>
        /// Returns StockPositionRecord from one line
        /// </summary>
        private StockPositionRecord Parse(string line)
        {
            var fields = GetFieldsSafe(line);
            if (fields.Count != FieldsCount)
            {
                throw new ArgumentException($"Incorrect number of fields when parsing {line} " +
                    $"to delimited fields. Expected count: {FieldsCount}");
            }

            DateTime date = DateTime.ParseExact(fields[0], "MM/dd/yyyy", null);
            string companyName = fields[2];
            string ticker = fields[3];
            int shares = int.Parse(fields[5], NumberStyles.Integer | NumberStyles.AllowThousands, 
                CultureInfo.InvariantCulture); // invariant culture specifies comma as thousands separator
            string weightWithoutPercentSign = fields[7].Replace("%", string.Empty);
            double weightPercent = double.Parse(weightWithoutPercentSign, NumberStyles.AllowDecimalPoint, 
                CultureInfo.InvariantCulture /* TODO: check if allows dot as decimal separator */);
            var result = new StockPositionRecord(date, companyName, ticker, shares, weightPercent);
            return result;
        }

        /// <summary>
        /// Throws <see cref="ArgumentException"/> if csv line parsing into token fails.
        /// Parse line into field separated by comma
        /// </summary>
        /// <param name="line"></param>
        /// <returns> field </returns>
        /// <exception cref="ArgumentException"></exception>
        private List<string> GetFieldsSafe(string line)
        {
            try
            {
                var fields = ParseIntoFields(line);
                return fields;
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Could not parse line {line} to delimited fields. See inner exception for details.", ex);
            }
        }
    }
}